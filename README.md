# Fantasy Grounds ruleset for Beyond the Wall

## What is this

This is a simple ruleset for the [ Fantasy Grounds ](http://fantasygrounds.com/) virtual tabletop, with rules for the game [Beyond the Wall and Other Adventures ](https://www.flatlandgames.com/btw/)by Flatland Games.

This is a very simple and initial version of this ruleset. At the moment:

### General:
 * Character sheet with mostly automated fields.
 * Rolls and tests for Saving Throws, attribute and skill tests.
 * Rolls for attack and damage on weapon list.

### GM/Combat:
 * Combat Tracker with linked fields: initiative, wounds, hp and AC.
 * NPC sheets with fields for the book bestiary format + automatic Saving Throw values based on hit dice.

## Download and installation
Download this repository as a `.zip` file, or one of the releases on the releases page, change the file extension from `.zip` to `.pak` and put the file on the `rulesets/` directory of your Fantasy Grounds installation.

This ruleset builds heavily on top of the FG included _Core RPG_ ruleset, so it needs to be installed (but it comes installed already with the most recent FG installations).

## Screenshots with info:
This is the main character sheet, where most of the info is organized. It is sized to ensure it will work on small screens like laptops.

There's a [quick video](https://youtu.be/hlXb7CUgdCM) available with some examples of usage for the character sheet.

Main Character Sheet:  
![Main Charactersheet](https://i.imgur.com/sQxx9NJ.png)
<dl>
<dt>Header</dt>
<dd>Has the basic player and character info. The Experience field is used to calculate the current level when you have a Class selected (this is the only way to control the character level).</dd>
<dt>Attributes</dt>
<dd>Filling in the value will calculate the modifier field. The value fields can be clicked to trigger a dice roll/attribute test (that is true for any field with a dice icon on the bottom left corner).</dd>
<dt>Saving Throws</dt>
<dd>Those are automatically calculated based on class/level. Double click triggers a Saving Throw dice roll.</dd>
<dt>Test difficult panel</dt>
<dd>That panel is there for convenience on Attribute or other tests. Double clicking the specific value adds that modifier to the Fantasy Ground mod stack (the bottom left of the screen box) and it affects the next dice rolled.</dd>
<dt>Overview panel</dt>
<dd>On this panel you can click the box to choose your class. Doing that automatically fills in the Base Attack Bonus (BAB), Initiative (Init) and Hit Dice fields. If the class chosen is _Multiclass_, a button will appear to allow access to the multiclass option panel.<br>The option button near the Armor Class shield allows you to add the many AC bonuses to get the final AC.<br>Fortune Points, Hit Points and Wounds can be filled manually. Double clicking the Fortune Points field causes a single point to be spent with a corresponding message on the chat.</dd>
<dt>Multiclass options (only shows off if multiclass is selected)</dt>
<dd>Use this panel to pick and choose the multiclass characteristics like what experience table or hit dice will be used.</dd>
<dt>Weapons Frame</dt>
<dd>Add weapons clicking the _Edit_ button on the left side and then the _Add_ button that appears. The weapon fields are not automatically calculated and need to be filled by hand. Double cliking the _Hit_ field rolls an attack with the hit modifier, and the same for the _Dmg_ field.</dd>
<dt>Cantrips Frames</dt>
<dd>Can be edited as the Weapon list. Use the attribute button to choose what attribute to use on that Cantrip's test. You can trigger a Cantrip test double clicking the dice to the right of the Cantrip's list entry.</dd>
</dl>

Using the right side tabs you can get to the other parts of the sheet. Next one is the _Abilities_ sheet.

![Abilities sheet](https://i.imgur.com/p1NjNlD.png)

The _Skills_ frame can be edited as the other lists on the sheet. Add in the skill name, choose a Training Bonus (TB) between +2 or +4, and indicate what attribute is to be used with that skill. Double click the dice symbol to throw a skill test with the selected bonus and attribute.
The Test difficulty levels are repeated again here for convenience.

There's a simple list for Traits, and a bigger free text area for other information like Spells, Rituals or Class Abilities.

![Inventory](https://i.imgur.com/mWaqWeJ.png)

The Inventory tab contains an editable list for items, as well as space for coinage/treasure and other possessions.


![Notes](https://i.imgur.com/IPbgtxA.png)

The Notes tab is simple and self-explanatory.

### GM Stuff

![Imgur](https://i.imgur.com/4RBXIgU.png)

The NPC panel follows the book bestiary field requirements. The Hit Dice field is used to calculate the initiative and Saving Throws, and can be clicked to randomly define the NPC _Hit Point_ value. Those fields can be manually edited too.

As in the Character Sheet, the _Attack_ and _Damage_ fields can be double clicked to execute the specific rolls. At the moment, the Saving Throws are not automatic, but are shown here for convenience.


![Imgur](https://i.imgur.com/mCK1K2J.png)

The GM version of the _Combat Tracker_ includes linked fields (indicated by the bottom right chain icons) for AC, Wounds, HP and Initiative (called _Order_). The linked fields mean that editing the value on this screen will also change it on the player's Character Sheet. The _Wound_ field accepts damage roll results from the chat to be dragged and dropped to add to the current value.

The Player version of the _Combat Tracker_ is a very simplified version and has no extra fields aside from ally initiative order.


## Disclaimers and Licenses

This is a non official, fan made ruleset. Beyond the Wall and Other Adventures, its intellectual property, logos, names and imagery are property of [Flatland Games](https://www.flatlandgames.com/). 

Beyond the Wall is licensed by the Open Gaming License.

This ruleset's author is in no way affilated to Flatland Games or the Beyond the Wall team in any way.
