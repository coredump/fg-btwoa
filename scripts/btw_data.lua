-- TKTK

aAttrs = {};
aWarrior = {};
aRogue = {};
aMage = {};
aMonster = {};

aClasses = {
  warrior = aWarrior,
  rogue = aRogue,
  mage = aMage
}

aStats = {
  warrior = {
    init = 1,
    hd   = 'd10'
  },
  rogue   = {
    init = 2,
    hd   = 'd8'
  },
  mage    = {
    init = 0,
    hd   = 'd6' 
  }
}

aAttrSwitch = {
  v_str = 'strength',
  v_dex = 'dexterity',
  v_con = 'constitution',
  v_int = 'intelligence',
  v_wis = 'wisdom',
  v_cha = 'charisma'
}

aSaveSwitch = {
  save_poison = 'poison',
  save_breath = 'breath weapon',
  save_poly = 'polymorph',
  save_spell = 'spell',
  save_item = 'magic item'
}

function onInit()

  -- Attribute mods

  -- Lua is a weird and indexes on 1
  aAttrs[1] = -4
  aAttrs[2] = -3
  aAttrs[3] = -3
  aAttrs[4] = -2
  aAttrs[5] = -2
  aAttrs[6] = -1
  aAttrs[7] = -1
  aAttrs[8] = -1
  aAttrs[9] = 0
  aAttrs[10] = 0
  aAttrs[11] = 0
  aAttrs[12] = 0
  aAttrs[13] = 1
  aAttrs[14] = 1
  aAttrs[15] = 1
  aAttrs[16] = 2
  aAttrs[17] = 2
  aAttrs[18] = 3
  aAttrs[19] = 3

  --                   XP, BAB, Poison, Breath, Poly, Spell, Item
  aWarrior[1]  = {      0,  1,   14,     17,     15,   17,    16 };
  aWarrior[2]  = {   2000,  2,   14,     17,     15,   17,    16 };
  aWarrior[3]  = {   4000,  3,   13,     16,     14,   14,    15 };
  aWarrior[4]  = {   8000,  4,   13,     16,     14,   14,    15 };
  aWarrior[5]  = {  16000,  5,   11,     14,     12,   12,    13 };
  aWarrior[6]  = {  32000,  6,   11,     14,     12,   12,    13 };
  aWarrior[7]  = {  64000,  7,   10,     13,     11,   11,    12 };
  aWarrior[8]  = { 120000,  8,   10,     13,     11,   11,    12 };
  aWarrior[9]  = { 240000,  9,    8,     11,      9,    9,    10 };
  aWarrior[10] = { 360000, 10,    8,     11,      9,    9,    10 };

  --                   XP, BAB, Poison, Breath, Poly, Spell, Item
  aRogue[1]  = {      0,  0,   13,     16,     12,   15,    14 };
  aRogue[2]  = {   1500,  1,   13,     16,     12,   15,    14 };      
  aRogue[3]  = {   3000,  1,   13,     16,     12,   15,    14 };
  aRogue[4]  = {   6000,  2,   13,     16,     12,   15,    14 };
  aRogue[5]  = {  12000,  3,   12,     15,     11,   13,    12 };
  aRogue[6]  = {  25000,  3,   12,     15,     11,   13,    12 };
  aRogue[7]  = {  50000,  4,   12,     15,     11,   13,    12 };
  aRogue[8]  = { 100000,  5,   12,     15,     11,   13,    12 };
  aRogue[9]  = { 200000,  5,   11,     14,      9,   11,    10 };
  aRogue[10] = { 300000,  6,   11,     14,      9,   11,    10 };

  --                XP, BAB, Poison, Breath, Poly, Spell, Item
  aMage[1]  = {      0,  0,   14,     15,     13,   12,    11 };
  aMage[2]  = {   2500,  1,   14,     15,     13,   12,    11 };   
  aMage[3]  = {   5000,  1,   14,     15,     13,   12,    11 };
  aMage[4]  = {  10000,  2,   14,     15,     13,   12,    11 };
  aMage[5]  = {  20000,  2,   14,     15,     13,   12,    11 };
  aMage[6]  = {  40000,  3,   13,     13,     11,   10,     9 };
  aMage[7]  = {  80000,  3,   13,     13,     11,   10,     9 };
  aMage[8]  = { 150000,  4,   13,     13,     11,   10,     9 };
  aMage[9]  = { 300000,  4,   13,     13,     11,   10,     9 };
  aMage[10] = { 450000,  5,   13,     13,     11,   10,     9 };

  
  aMonster[1]  = { 14,     17,     15,   17,    16 };
  aMonster[2]  = { 14,     17,     15,   17,    16 };
  aMonster[3]  = { 13,     16,     14,   14,    15 };
  aMonster[4]  = { 13,     16,     14,   14,    15 };
  aMonster[5]  = { 11,     14,     12,   12,    13 };
  aMonster[6]  = { 11,     14,     12,   12,    13 };
  aMonster[7]  = { 10,     13,     11,   11,    12 };
  aMonster[8]  = { 10,     13,     11,   11,    12 };
  aMonster[9]  = {  8,     11,      9,    9,    10 };
  aMonster[10] = {  8,     11,      9,    9,    10 };
  aMonster[11] = {  7,     10,      8,    8,    9 };
  aMonster[12] = {  7,     10,      8,    8,    9 };
  aMonster[13] = {  5,     8,      6,    5,    7 };
  aMonster[14] = {  5,     8,      6,    5,    7 };
  aMonster[15] = {  4,     7,      5,    4,    6 };
  aMonster[16] = {  4,     7,      5,    4,    6 };
  aMonster[17] = {  3,     6,      4,    4,    5 };
  aMonster[18] = {  3,     6,      4,    4,    5 };
  aMonster[19] = {  3,     6,      4,    4,    5 };
  aMonster[20] = {  3,     6,      4,    4,    5 };

end