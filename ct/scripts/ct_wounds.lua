function handleDrop(draginfo)
	sType = draginfo.getType();

	if sType == 'number' or sType == 'damage' then
		nValue = draginfo.getNumberData();
		nCurValue = getValue()
		setValue(nCurValue + nValue);
		return true
	end
end