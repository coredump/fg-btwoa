function onInit()
  if super and super.onInit then
    super.onInit()
  end
 
  node = window.getDatabaseNode();
  DB.addHandler(node.getPath('hd'), 'onUpdate', updateInit);
end

function updateInit(node)
  sHd = DB.getValue(window.getDatabaseNode(), '.hd');
  if sHd == '' then
    return true
  end

  aData, _ = StringManager.split(sHd, 'd');

  setValue(aData[1]);
end