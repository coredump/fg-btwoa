function onDoubleClick()
  local gmid, isgm = GmIdentityManager.getCurrent();
  if not isgm then
    return true
  end
  local nodeChar = window.getDatabaseNode();

  aDice, nMod = StringManager.convertStringToDice(DB.getValue(nodeChar, '.hd'));

  result = StringManager.evalDice(aDice, nMod);

  DB.setValue(nodeChar, '.hp', 'number', result);

end
