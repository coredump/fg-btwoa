function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  aDice = StringManager.convertStringToDice('1d20');
  nMod = getValue()
  sDesc = "Rolling to attack with " .. sName .. '.';

	local rRoll = { sType = "dice", sDesc = sDesc, aDice = aDice, nMod = nMod };
  ActionsManager.actionDirect(nil, "dice", { rRoll });

end
