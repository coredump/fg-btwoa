aWindows = {};

function D(mess)
  if BTWOptions.Bdebug then
    Debug.chat(mess);
  end  
end

function findTopNode(node)
  -- Tries to find the DB root node
  if string.match(node.getPath(),'^charsheet%.id%-%d+$') then
    return node;
  else
    return findTopNode(node.getParent());
  end
end

function getClass(node, stat)
  -- Gets a class for a specific stat
  class = DB.getValue(node, 'class');
  if class ~= 'multiclass' then
    return class;
  else
    ret = DB.getValue(node, 'multiclass.options.' .. stat);
    -- The stringcycler sends an empty value on the 
    -- first item, so we just return warrior
    if ret == '' or ret == nil then
      ret = 'warrior';
    end
    return ret;
  end
end

function setLevelFromXP(node)
  local xp = DB.getValue(node, 'experience');
  local class = getClass(node, 'xp');

  D(node);
  D('Current: ' .. xp);
  currentLevel = 1;

  if class == '' or class == nil then
    class = 'warrior'
  end

  for i, v in ipairs(BTWData.aClasses[class]) do
    if xp == '' then
      xp = 0;
    end
    if tonumber(xp) >= v[1] then
      currentLevel = i;
      if currentLevel == 10 then
        DB.setValue(node, 'overview.level', 'number', currentLevel);
        return currentLevel;
      end
    else
      DB.setValue(node, 'overview.level', 'number', currentLevel);
      return currentLevel;
    end
  end

end

function sortCTByInit(w1, w2)
	i1 = DB.getValue(w1, 'initiative');
	i2 = DB.getValue(w2, 'initiative');
	return i1 > i2;
end

function updateXPHandler(window)
  node = findTopNode(window.getDatabaseNode());
  updateViews(node, window);
end

function updateAttributeHandler(node, window)
  D('char_main_btw updateAttributeHandler');

  nScore = DB.getValue(node, '');
  nMod = DB.getValue(node, '..mod');

  DB.setValue(node, '..mod', 'number', BTWData.aAttrs[nScore]);
  updateViews(node, window);
end

function updateViews(node, window)
  -- will be called to update stats when other stuff changes
  topNode = findTopNode(node);
  D('Entering UpdateViews');

  -- set the level from XP first
  level = setLevelFromXP(topNode);
  class = DB.getValue(topNode, 'class');
  D('Class: ' .. class );
  D('Level: ' .. level);

  if window == nil then
    dataPath = topNode.getPath();
    local window = Interface.findWindow('charsheet_main', dataPath);
  end
  D(window);

  if class == '' then
    aWindows['charsheet_main'].button_mc_options.setVisible(false);
    return;  
  end

  if class == 'multiclass' then
    aWindows['charsheet_main'].button_mc_options.setVisible(true);
  else
    aWindows['charsheet_main'].button_mc_options.setVisible(false);
  end

  setBAB(topNode, level);
  setInit(topNode, level);
  setNextLevel(topNode, level);
  setHitDice(topNode, level);
  setSaves(topNode, level);
end

function classUpdate(node)
  updateViews(node);
end

function setBAB(node, level)
  local class = getClass(node, 'bab');
  if class == '' then
    return
  end

  local nValue = BTWData.aClasses[class][level][2];
  D('New BAB: ' .. nValue);
  DB.setValue(node, 'overview.bab', 'number', nValue);
end

function setInit(node, level)
  local class = getClass(node, 'init');

  if class == '' or class == nil then
    class = 'warrior';
  end

  dexMod = DB.getValue(node, 'abilities.dex.mod');
  classBonus = BTWData.aStats[class]['init']; 
  local nValue = classBonus + dexMod + level;
  D('New init: ' .. nValue);
  DB.setValue(node, 'overview.initiative', 'number', nValue);
end

function setNextLevel(node, level)
  local  class = getClass(node, 'xp');
  if class == '' or class == nil then
  class = 'warrior'
  end

  if level > 9 then
    nValue = 'MAX';
  else
    nValue = BTWData.aClasses[class][level+1][1];
  end
  DB.setValue(node, 'overview.nextLevelXP', 'string', nValue);
end

function setHitDice(node, level)
  local class = getClass(node, 'hitdice');
  if class == '' or class == nil then
    class = 'warrior';
  end

  local nValue = BTWData.aStats[class]['hd'];
  D('Dice: ' .. nValue);

  dice, mod = StringManager.convertStringToDice(nValue);
  DB.setValue(node, 'overview.hitDice', 'dice', dice);
end

function setSaves(node, level)
  local class = getClass(node, 'saves');

  D('setSaves');
  saveList = {'poison', 'breath', 'poly', 'spell', 'item'};

  item = 3;
  for _, save in ipairs(saveList) do
    nValue = BTWData.aClasses[class][level][item];
    DB.setValue(node, 'saves.' .. save, 'number', nValue)    
    item = item + 1;
  end
end