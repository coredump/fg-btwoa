function onDoubleClick(x, y)
  -- Got from MoreCore by damned. Simplified to fortune points.

  cValue = getValue();  -- Avoid caling getValue too many times.

  if cValue > 0 then
    local nodeWin = window.getDatabaseNode();
    local rActor = ActorManager.getActor('pc', nodeWin);
    local sHeroType = 'Fortune Point';
    local msg = {icon = 'spell_cast', font = 'msgfont'}; -- spell_cast from Core
    msg.text = rActor.sName .. ' is using a ' .. sHeroType;
    Comm.deliverChatMessage(msg);
    setValue(cValue -1);
  end
  return true;
end        