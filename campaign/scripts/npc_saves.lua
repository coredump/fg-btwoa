function onInit()
  if super and super.onInit then
    super.onInit()
  end
 
  node = window.getDatabaseNode();
  DB.addHandler(node.getPath('hd'), 'onUpdate', updateST);
  updateST(window.getDatabaseNode());
end

function updateST(node)
  sHd = DB.getValue(window.getDatabaseNode(), '.hd');
  if sHd == '' then
    return true
  end

  aData, _ = StringManager.split(sHd, 'd');
  aVals = BTWData.aMonster[tonumber(aData[1])];

  sText = 'Poison: ' .. aVals[1] .. '\nBreath Weapon: ' .. aVals[2] .. '\nPolymorph: ' .. aVals[3] .. '\nSpell: ' .. aVals[4] .. '\nMagic Item: ' .. aVals[5];

  setValue(sText);
end

function onDoubleClick()
  local gmid, isgm = GmIdentityManager.getCurrent();
  if not isgm then
    return true
  end

  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  aDice = StringManager.convertStringToDice('1d20');
  sDesc = "Rolling save for " .. sName .. '.';
  nMod = 0

	local rRoll = { sType = "dice", sDesc = sDesc, aDice = aDice, nMod = nMod, bSecret = true };
  ActionsManager.actionDirect(nil, "dice", { rRoll });
end