function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  dice = StringManager.convertStringToDice('1d20');
  saveName = BTWData.aSaveSwitch[getName()];
  
  sDesc = 'Save against ' .. StringManager.capitalizeAll(saveName) .. '.';
  nSave = getValue();

  local rRoll = { sType = "sthrow", sDesc = sDesc, aDice = dice, nSave = nSave,nMod = 0, aOp = '>' };
  ActionsManager.performAction(nil, nil, rRoll);
end
