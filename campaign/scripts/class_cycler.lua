--

function onInit()
  if super and super.onInit then
    super.onInit()
  end
  
  if window.class.getValue() == 'Multiclass' then
    window.button_mc_options.setVisible(true);
  end
end

function onValueChanged()
  BTWLib.classUpdate(window.getDatabaseNode());
end