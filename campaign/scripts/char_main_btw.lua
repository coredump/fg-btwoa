--[[


]]--

function onInit()
  if BTWOptions.Bdebug then
    Debug.chat('char_main_btw onInit');
  end

  if User.isLocal() then
    speak.setVisible(false);
    portrait.setVisible(false);
    localportrait.setVisible(true);
  end

  -- set some defaults value before everything
  DB.setValue(nodeChar, 'defenses.base', 'number', 10);
end
