function onInit()
  if super and super.onInit then
    super.onInit()
  end
end

function onDoubleClick()
  local gmid, isgm = GmIdentityManager.getCurrent();
  if not isgm then
    return true
  end
  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');

  sDamageField = DB.getValue(nodeChar, '.damage');
  split, stats = StringManager.split(sDamageField, ' ');
  sDamage = split[1];

  aDice, nMod = StringManager.convertStringToDice(sDamage);
  sDesc = StringManager.combine(' ', 'Rolling damage for', sName, split[2], '.');

  local rActor = ActorManager.getActor('pc', nodeWin);
  local rRoll = { sType = "damage", sDesc = sDesc, aDice = aDice, nMod = nMod, bSecret = false };

  ActionsManager.actionDirect(nil, rActor, { rRoll });

end
