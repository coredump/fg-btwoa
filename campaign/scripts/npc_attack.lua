function onInit()
  if super and super.onInit then
    super.onInit()
  end
end

function onDoubleClick()
  local gmid, isgm = GmIdentityManager.getCurrent();
  if not isgm then
    return true
  end

  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  aDice = StringManager.convertStringToDice('1d20');
  nMod = DB.getValue(nodeChar, '.attack');
  sDesc = "Rolling attack for " .. sName .. '.';

	local rRoll = { sType = "dice", sDesc = sDesc, aDice = aDice, nMod = nMod };
  ActionsManager.actionDirect(nil, "dice", { rRoll });

end
