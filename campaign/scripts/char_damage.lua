function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  aDice = DB.getValue(nodeChar, '.damage_dice');
  nMod = getValue()
  sDesc = "Rolling damage for " .. sName .. '.';

  local rActor = ActorManager.getActor('pc', nodeWin);
  local rRoll = { sType = "damage", sDesc = sDesc, aDice = aDice, nMod = nMod };

  ActionsManager.actionDirect(nil, rActor, { rRoll });

end
