function onValueChanged()
  node = getDatabaseNode();
  BTWLib.updateAttributeHandler(node, window);
  return true
end

function onInit()
  if super and super.onInit then
    super.onInit();
  end

  BTWLib.aWindows['charsheet_main'] = window;
end

function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  dice = StringManager.convertStringToDice('1d20');
  attrName = BTWData.aAttrSwitch[getName()];
  
  sDesc = StringManager.combine(' ', StringManager.capitalizeAll(attrName), 'test.');
  nSave = getValue();

  local rRoll = { sType = "sthrow", sDesc = sDesc, aDice = dice, nSave = nSave,nMod = 0, aOp = '<' };
  ActionsManager.performAction(nil, nil, rRoll);
end
