function onInit()
  if super and super.onInit then
    super.onInit();
  end

  setDice(StringManager.convertStringToDice('1d20'));
end

function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  sAttr = DB.getValue(nodeChar, '.attribute');
  nSave = DB.getValue(nodeChar, '...abilities.' .. sAttr .. '.score');
  aDice = StringManager.convertStringToDice('1d20');
  sDesc = 'Casting cantrip ' .. sName .. ' (using ' .. sAttr .. ').';

  if nSave == nil or sName == '' then
    return true
  end

  local rRoll = { sType = 'sthrow', sDesc = sDesc, aDice = aDice, nSave = nSave,nMod = 0, aOp = '<' };
  ActionsManager.performAction(nil, nil, rRoll);

end
