function onInit()
  if super and super.onInit then
    super.onInit();
  end

  setDice(StringManager.convertStringToDice('1d20'));
end

function onDoubleClick()
  local nodeChar = window.getDatabaseNode();

  sName = DB.getValue(nodeChar, '.name');
  sAttr = DB.getValue(nodeChar, '.attribute');
  nSave = DB.getValue(nodeChar, '...abilities.' .. sAttr .. '.score');
  aDice = StringManager.convertStringToDice('1d20');
  nMod  = DB.getValue(nodeChar, '.training');
  sDesc = 'Rolling ' .. sName .. ' skill test (using ' .. sAttr .. ').\n[+' .. nMod .. ' training bonus]';

  if nSave == nil or sName == '' then
    return true
  end

  local rRoll = { sType = 'sthrow', sDesc = sDesc, aDice = aDice, nSave = nSave,nMod = nMod, aOp = '<' };
  ActionsManager.performAction(nil, nil, rRoll);

end
